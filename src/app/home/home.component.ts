import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private dataService: DataService) { }

  public list = [];
  public shouldDisplay:boolean = false;
  public mode:string;

  public todoTitle:string = '';
  public todoBody:string = '';

  public targetIndex:any;

  ngOnInit() {
    this.list = this.dataService.fetchList();
  }

  triggerModal(type, index?){

    switch(type){
      case 'add':
        this.todoBody = '';
        this.todoTitle = '';
        break;
      case 'edit':
        this.todoTitle = this.list[index].title;
        this.todoBody = this.list[index].body;
        break;
    }

    if(index !== null || index !== undefined){
      this.targetIndex = index;
    }
    this.mode = type;
    this.shouldDisplay = true;

    
  }

  dismissModal(){
    this.shouldDisplay = false;
  }

  addTodo(){
    this.dataService.addToList({
      title: this.todoTitle,
      body: this.todoBody
    });
    this.todoTitle = '';
    this.todoBody = '';
    this.dismissModal();
  }

  deleteTodo(index){
    console.log("deleting from "+index);
    if(this.targetIndex){
      index = this.targetIndex
    }
    this.dataService.deleteItem(index);
    this.targetIndex = null;
    this.dismissModal();
  }

  updateTodo(index){
    let data = {title: this.todoTitle, body: this.todoBody};
    this.dataService.updateTodo(index, data);
    this.dismissModal();
  }

}
